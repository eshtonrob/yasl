/* main.cc */

#include "heading.h"
#include "ast.h"
#include "codegen.h"
#include "list.h"

// prototype of bison-generated parser function
 extern int yyparse(tree **ptree, hashmap **sm);
 int getLinecount (char *fl);

 int main(int argc, char **argv)
 {
	
   //int lc = getLinecount(argv[1]); printf("%d\n", lc);

  if ((argc > 1) && (freopen(argv[1], "r", stdin) == NULL))
  {
    printf("%s",argv[0]);
    printf(": File ");
    printf("%s",argv[1]);
    printf(" cannot be opened.\n");
    printf("Exit\n");

	return 1;
  }
  
  hashmap sym_table;
  hashmap_init(&sym_table, hashmap_hash_string, hashmap_compare_string, 0);
  hashmap *smy = &sym_table;
  bool recurs;
 
  tree *ast = (tree*) malloc (sizeof(tree));    

  printf("PRE_PARSE\n");
  yyparse(&ast, &smy);
  printf("POS_PARSE\n");

  //type_check(ast);
  node *src = gen_yir(ast, smy, &recurs);

  printf("LAMBDA_GEN\n");
  node *lsrc = gen_lambda(src, recurs); 

  printf("IR OUTPUT\n");

  sds flname = sdsnew(argv[1]);
  sds *tokens; 
    int count;

   tokens = sdssplitlen(flname,sdslen(flname),".",1,&count); // split on period
    
  sds fname = sdsdup(tokens[0]);

  fname = sdscat(fname, ".lam");
  
  FILE *f = fopen(fname, "w");

   if (f == NULL)
   {
    printf("Error opening file!\n");
    exit(1);
  }

  while(lsrc){fprintf(f, "%s\n", lsrc->data); lsrc = lsrc->next;}

  fclose(f);
  sdsfree(flname);
  sdsfree(fname);
  sdsfreesplitres(tokens,count);
  hashmap_destroy(&sym_table);
  free(src);
  free(lsrc);
  free(ast); 

  return 0;
 }

 int getLinecount (char *fl) {
    int count = 0;
    FILE *fp = fopen(fl, "r");
    if(fp == NULL)
    {
        return -1;
    }

   //stdin = fp;

    while (EOF != (fscanf(fp,"%*[^\n]"), fscanf(fp,"%*c"))) 
    {
            count++;
    }
    fclose(fp); 
    return count;
 }
