%{
#include <stdbool.h>
#include "tree.h"


typedef struct hashmap hashmap;

void yyerror(tree **ptree, hashmap **sm, const char *s);
int yylex(void);
const char *sym_hashmap_put(struct hashmap *map, const char *key, const char *data);



%}

%parse-param {tree **ptree}
%parse-param {hashmap **sym}

%union{
	int int_val;
	double real_val;
	bool bool_val;
	char * id;
	tree * a_tree;
 }

%start program

%token	<int_val>	INTEGER_LITERAL
%token	<real_val>	FLOAT_LITERAL
%token	<id>		ID
%token  <bool_val>	BOOLEAN_LITERAL

%type   <a_tree>	expression term factor NIL
%type   <a_tree>	statement match_expression 
%type   <a_tree>	function_definition expression_definition if_conditional 
%type   <a_tree>	pattern_matching list_ops conditional_expression 	
%type   <a_tree>	statement_list match_expression_list expression_list

%token IF THEN ELSE
%token DO
%token END
%token CASE
%token LET
%token DEF
%token HEAD
%token TAIL
%token PAIR
%token NIL
%token NUL
%token CIFR
%token MATCH
%token COLON ALL
%token INT FLOAT BOOL
%token TOINT TOFLOAT IN INB INH OUT OUTB OUTH HALT //builtins
%token LPAR RPAR LBRAC RBRAC

%nonassoc ID
%right  UMINUS INIT UPLUS
%left   EQ NOTEQ LT GT AND OR
%left	PLUS MINUS
%left	TIMES DIVIDE MODULO 
%left 	FPLUS FMINUS
%left	FTIMES FDIVIDE
%left   LPAR RPAR



%%

program
      : statement_list							{*ptree=$1;}
      ;									
	  /*Statement is a synonym for complex expression*/
statement 
	: function_definition						
	| expression_definition
	| if_conditional						
	| pattern_matching
	| list_ops
	| expression
	;


statement_list
	      : statement						{$$ = $1;} 
	      | statement_list statement				{$$ = make_operator($1,";;", $2);} /*transform list into binary tree*/
	      ;


function_definition						        /* ID expr_list.ret_expr*** annotate node with reported type**/
		  : DEF INT ID expression_list CIFR statement	       {sym_hashmap_put(*sym, $3, "func"); tree *func = make_operator($4,$3,$6); func->s_type = t_int; $$ = func;} 
		  | DEF FLOAT ID expression_list CIFR statement        {sym_hashmap_put(*sym, $3, "func"); tree *func = make_operator($4,$3,$6); func->s_type = t_float; $$ = func;} 
		  | DEF BOOL ID expression_list CIFR statement	       {sym_hashmap_put(*sym, $3, "func"); tree *func = make_operator($4,$3,$6); func->s_type = t_bool; $$ = func;}
		  | DEF INT ID NIL CIFR statement	               {sym_hashmap_put(*sym, $3, "func"); tree *func = make_operator($4,$3,$6); func->s_type = t_int; $$ = func;} 
		  | DEF FLOAT ID NIL CIFR statement                    {sym_hashmap_put(*sym, $3, "func"); tree *func = make_operator($4,$3,$6); func->s_type = t_float; $$ = func;} 
		  | DEF BOOL ID NIL CIFR statement	               {sym_hashmap_put(*sym, $3, "func"); tree *func = make_operator($4,$3,$6); func->s_type = t_bool; $$ = func;}  
		  ;


expression_definition 
		    : LET ID INIT expression				{sym_hashmap_put(*sym, $2, "var"); $$ = make_alias($2,$4);} /* let x = exp*/
		    | LET ID INIT list_ops				{sym_hashmap_put(*sym, $2, "list"); $$ = make_alias($2,$4);}
		    ; 

if_conditional 
	     : IF conditional_expression THEN statement ELSE statement	{$$ = make_if_else($2, $4, $6);}
	     ;


expression_list
	      : expression_list expression				{$$ = make_operator($1,";", $2);} /*transform list into binary tree*/
	      | expression						{$$ = $1;}
	      ;

conditional_expression
		     : statement GT statement				{$$ = make_operator($1, ">", $3);}
		     | statement LT statement				{$$ = make_operator($1, "<", $3);}
		     | statement EQ statement				{$$ = make_operator($1, "==", $3);}
		     | statement NOTEQ statement			{$$ = make_operator($1, "!", $3);}
		     | statement OR statement				{$$ = make_operator($1, "|", $3);}
		     | statement AND statement			        {$$ = make_operator($1, "&", $3);}
	 	     | NUL expression				        {$$ = make_operator(NULL, "NUL", $2);} /*check for nil list*/
		     ;


pattern_matching
	       : CASE statement COLON match_expression_list END	        {$$ = make_operator($2,"@", $4);} /*should apply exp1 to every exp1i in the list and determine the result*/
	       ;  										          /*I.E the operation is a filter on the list*/


match_expression_list
		    : match_expression_list match_expression		{$$ = make_operator($1,"@@", $2);} /*transform list into binary tree*/
		    | match_expression					{$$ = $1;} 
		    ;

match_expression
	       : OR statement MATCH statement				{$$ = make_if($2,$4);}/*The toplevel expression will be applied*/
	       | OR conditional_expression MATCH statement		{$$ = make_if($2,$4);}
	       | OR ALL MATCH statement				        {$$ = $4;}
	       ;

list_ops
	: PAIR expression expression					{$$ = make_cons($2,$3);}/*needs to be typechecked*/
        | LBRAC expression COLON expression RBRAC			{$$ = make_cons($2,$4);}/*needs to be typechecked*/
	| HEAD expression						{$$ = make_operator(NULL, "HEAD", $2);}
	| TAIL expression						{$$ = make_operator(NULL, "TAIL", $2);}
	| NIL								{$$ = make_operator(NULL, "NIL", NULL);} /*NIL is the empty list*/
	;


expression 
	  : MINUS term PLUS term	{$$ = make_operator($4, "-", $2);} /*by commutative property*/
	  | MINUS term MINUS term	{$$ = make_operator(NULL, "-", make_operator ($2, "+", $4));} /*-A-B=-1(A+B)*/
	  | term MINUS term		{$$ = make_operator($1, "-", $3);}
	  | term PLUS term		{$$ = make_operator($1, "+", $3);}
	  | MINUS term  %prec UMINUS	{$$ = make_operator(NULL, "-", $2);}
	  | term %prec UPLUS		{$$ = $1;}
	  | term FPLUS term		{$$ = make_operator($1, "+:", $3);}
	  | FMINUS term FMINUS term	{$$ = make_operator(NULL, "-:", make_operator ($2, "+:", $4));} 
	  | term FMINUS term		{$$ = make_operator($1, "-:", $3);}
	  | FMINUS term %prec UMINUS	{$$ = make_operator(NULL, "-:", $2);}
	  ;

term 
	: factor TIMES factor		{$$ = make_operator($1, "*", $3);}
	| factor DIVIDE factor		{$$ = make_operator($1, "/", $3);}
	| factor MODULO factor		{$$ = make_operator($1, "%", $3);}
	| factor			{$$ = $1;}
	| factor FTIMES factor		{$$ = make_operator($1, "*:", $3);}
	| factor FDIVIDE factor		{$$ = make_operator($1, "/:", $3);}
	| TOFLOAT factor		{$$ = make_operator(NULL,"FLT_",$2); $$->s_type = t_float;} /*return a typed I combinator*/
	| TOINT factor			{$$ = make_operator(NULL,"INT_",$2);$$->s_type = t_int;} /*return a typed I combinator*/
	| IN factor			{$$ = make_operator(NULL,"IN",$2);}
	| INB factor			{$$ = make_operator(NULL,"INB",$2);}
	| INH factor			{$$ = make_operator(NULL,"INH",$2);}
	| OUT factor			{$$ = make_operator(NULL,"OUT",$2);}
	| OUTB factor			{$$ = make_operator(NULL,"OUTB",$2);}
	| OUTH factor			{$$ = make_operator(NULL,"OUTH",$2);}
	| HALT factor			{$$ = make_operator(NULL,"HALT",$2);}
	;

factor 
	: ID				{sym_hashmap_put(*sym, $1, "var"); $$ = make_variable($1);} //insert variable into symbol table
	| INTEGER_LITERAL		{$$ = make_int($1);}
	| FLOAT_LITERAL			{$$ = make_real($1);}
	| BOOLEAN_LITERAL		{$$ = make_bool($1);}
	| ID LPAR expression_list RPAR  {$$ = make_operator($3, $1, NULL);} /*Create a call node with the provided parameters, evaluation is delayed*/
	| LPAR statement RPAR		{$$ = $2;}	
	; 

%%


void yyerror(tree **ptree, hashmap **sm, const char *s)
{

  extern int yylineno;	// defined and maintained in lex.c
  extern char *yytext;	// defined and maintained in lex.c
	
  printf("%s", s);
  printf(" at symbol \"");
  printf("%s", yytext);
  printf("\" on line ");
  printf("%d\n", yylineno);
  exit(1);
 }

int yywrap() {
	return(1);
 }



