/* Yasl */
/* yasl.lex */

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "tree.h"
#include "yasl.tab.h"

 typedef struct hashmap hashmap;

extern void yyerror(tree **ptree, hashmap **sm, const char *s);
static void comment(void);
static void seq_block(void);

//int yylineno = 1;
%}

%option nounistd
%option nounput
%option never-interactive

digit		[0-9]
carac		[a-zA-Z_0-9]
carach		[a-zA-Z][a-zA-Z0-9_]*
int_const	{digit}+
real 		({digit}+[.]{digit}*)|({digit}*[.]{digit}+)
exp 		({int_const}|{real})[eE]-?{int_const}


%%
"/*"            { comment(); }
"#c"		{ seq_block();}
"#asm"		{ seq_block();}

{int_const}	{yylval.int_val = atoi(yytext); return INTEGER_LITERAL;}
{real}|{exp}	{yylval.real_val = atof(yytext); return FLOAT_LITERAL;}
"+"		{return PLUS;}
"+:"		{return FPLUS;}
"-"		{return MINUS;}
"-:"		{return FMINUS;}
"*"		{return TIMES;}
"*:"		{return FTIMES;}
"%"		{return MODULO;}
"/"		{return DIVIDE;}
"/:"		{return FDIVIDE;}
"("		{return LPAR;}
")"		{return RPAR;}
"if"		{return IF;}
"then"		{return THEN;}
"else"		{return ELSE;}
"do"		{return DO;}
"end"		{return END;}
"case"		{return CASE;}
"let"		{return LET;}
"def" 		{return DEF;}
"true"		{yylval.bool_val = true; return BOOLEAN_LITERAL;}
"false"		{yylval.bool_val = false; return BOOLEAN_LITERAL;}
"int_"		{return TOINT;}
"float_"	{return TOFLOAT;}
"int"		{return INT;}
"float"		{return FLOAT;}
"bool"		{return BOOL;}
":"		{return COLON;}
"="		{return INIT;}
"=="		{return EQ;}
">"		{return GT;}
"<"		{return LT;}
"&"		{return AND;}
"|"		{return OR;}
"!"		{return NOTEQ;}
"$"		{return CIFR;}
"@"		{return MATCH;}
"_"		{return ALL;}
"["		{return LBRAC;}
"]"		{return RBRAC;}
"head"		{return HEAD;}
"tail"		{return TAIL;}
"pair"		{return PAIR;}
"nil"		{return NIL;}
"null"		{return NUL;}
"in_"		{return IN;}
"in_b"		{return INB;}
"in_h"		{return INH;}
"out_"		{return OUT;}
"out_b"		{return OUTB;}
"out_h"		{return OUTH;}
"halt_"		{return HALT;}



[ ]*		{}
[\t]*		{}
[\n]		{ yylineno++;}

{carach}	{yylval.id =  malloc(sizeof(char) *
			(strlen(yytext)+1));
			ysl_strlcpy(yylval.id,yytext, (strlen(yytext)+1));return ID;} /*



Since the value of an IDENTIFIER is a string, we need to set the string value field of the yylval variable.
The variable yytext is a buffer used by lex – the next time a string is matched, the contents of the buffer
will be overwritten by the new string. Thus, we cannot just set yylval.string_value = yyext – instead
we need to allocate space for yylval.string value and copy in the value of yytext. Remember that C strings
are null terminated – so we need to set aside an extra unit of space for the termination character ’\0’.*/



.		{yyerror(NULL, NULL, "SCANNER: Unrecognized character"); exit(1);	}

%%

static void comment(void)
{
    int c;

    while ((c = input()) != 0)
        if (c == '*')
        {
            while ((c = input()) == '*')
                ;

            if (c == '/')
                return;

            if (c == 0)
                break;
        }
    yyerror(NULL, NULL, "Unterminated comment");
}


/*not implemented yet*/
static void seq_block(void)
{
int c;

while((c=input()) != 0)
	;

     // yyerror("Unterminated imperative block");
}
