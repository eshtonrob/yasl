/* original parser id follows */
/* yysccsid[] = "@(#)yaccpar	1.9 (Berkeley) 02/21/93" */
/* (use YYMAJOR/YYMINOR for ifdefs dependent on parser version) */

#define YYBYACC 1
#define YYMAJOR 1
#define YYMINOR 9
#define YYPATCH 20140715

#define YYEMPTY        (-1)
#define yyclearin      (yychar = YYEMPTY)
#define yyerrok        (yyerrflag = 0)
#define YYRECOVERING() (yyerrflag != 0)
#define YYENOMEM       (-2)
#define YYEOF          0
#define YYPREFIX "yy"

#define YYPURE 0

#line 2 "yasl.y"
#include <stdbool.h>
#include "tree.h"


typedef struct hashmap hashmap;

void yyerror(tree **ptree, hashmap **sm, const char *s);
int yylex(void);
const char *sym_hashmap_put(struct hashmap *map, const char *key, const char *data);



#line 19 "yasl.y"
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union{
	int int_val;
	double real_val;
	bool bool_val;
	char * id;
	tree * a_tree;
 } YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
#line 49 "yasl.tab.c"

/* compatibility with bison */
#ifdef YYPARSE_PARAM
/* compatibility with FreeBSD */
# ifdef YYPARSE_PARAM_TYPE
#  define YYPARSE_DECL() yyparse(YYPARSE_PARAM_TYPE YYPARSE_PARAM)
# else
#  define YYPARSE_DECL() yyparse(void *YYPARSE_PARAM)
# endif
#else
# define YYPARSE_DECL() yyparse(tree ** ptree, hashmap ** sym)
#endif

/* Parameters sent to lex. */
#ifdef YYLEX_PARAM
# define YYLEX_DECL() yylex(void *YYLEX_PARAM)
# define YYLEX yylex(YYLEX_PARAM)
#else
# define YYLEX_DECL() yylex(void)
# define YYLEX yylex()
#endif

/* Parameters sent to yyerror. */
#ifndef YYERROR_DECL
#define YYERROR_DECL() yyerror(tree ** ptree, hashmap ** sym, const char *s)
#endif
#ifndef YYERROR_CALL
#define YYERROR_CALL(msg) yyerror(ptree, sym, msg)
#endif

extern int YYPARSE_DECL();

#define INTEGER_LITERAL 257
#define FLOAT_LITERAL 258
#define ID 259
#define BOOLEAN_LITERAL 260
#define NIL 261
#define IF 262
#define THEN 263
#define ELSE 264
#define DO 265
#define END 266
#define CASE 267
#define LET 268
#define DEF 269
#define HEAD 270
#define TAIL 271
#define PAIR 272
#define NUL 273
#define CIFR 274
#define MATCH 275
#define COLON 276
#define ALL 277
#define INT 278
#define FLOAT 279
#define BOOL 280
#define TOINT 281
#define TOFLOAT 282
#define IN 283
#define INB 284
#define INH 285
#define OUT 286
#define OUTB 287
#define OUTH 288
#define HALT 289
#define LPAR 290
#define RPAR 291
#define LBRAC 292
#define RBRAC 293
#define UMINUS 294
#define INIT 295
#define UPLUS 296
#define EQ 297
#define NOTEQ 298
#define LT 299
#define GT 300
#define AND 301
#define OR 302
#define PLUS 303
#define MINUS 304
#define TIMES 305
#define DIVIDE 306
#define MODULO 307
#define FPLUS 308
#define FMINUS 309
#define FTIMES 310
#define FDIVIDE 311
#define YYERRCODE 256
typedef short YYINT;
static const YYINT yylhs[] = {                           -1,
    0,    4,    4,    4,    4,    4,    4,   12,   12,    6,
    6,    6,    6,    6,    6,    7,    7,    8,   14,   14,
   11,   11,   11,   11,   11,   11,   11,    9,   13,   13,
    5,    5,    5,   10,   10,   10,   10,   10,    1,    1,
    1,    1,    1,    1,    1,    1,    1,    1,    2,    2,
    2,    2,    2,    2,    2,    2,    2,    2,    2,    2,
    2,    2,    2,    3,    3,    3,    3,    3,    3,
};
static const YYINT yylen[] = {                            2,
    1,    1,    1,    1,    1,    1,    1,    1,    2,    6,
    6,    6,    6,    6,    6,    4,    4,    6,    2,    1,
    3,    3,    3,    3,    3,    3,    2,    5,    2,    1,
    4,    4,    4,    3,    5,    2,    2,    1,    4,    4,
    3,    3,    2,    1,    3,    4,    3,    2,    3,    3,
    3,    1,    3,    3,    2,    2,    2,    2,    2,    2,
    2,    2,    2,    1,    1,    1,    1,    4,    3,
};
static const YYINT yydefred[] = {                         0,
   65,   66,    0,   67,   38,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    7,    0,    0,    8,
    2,    3,    4,    5,    6,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,   36,   37,    0,   56,   55,
   57,   58,   59,   60,   61,   62,   63,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    9,   20,    0,   27,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,   34,   69,    0,    0,
    0,    0,   42,   41,   45,   47,   49,   50,   51,   53,
   54,   68,   19,   23,   24,   22,   21,   26,   25,    0,
    0,   30,    0,   16,   17,    0,    0,    0,    0,    0,
    0,    0,   39,   40,   46,    0,    0,    0,    0,   28,
   29,    0,    0,    0,    0,    0,    0,   35,   18,    0,
    0,    0,   13,   10,   14,   11,   15,   12,   33,   31,
   32,
};
static const YYINT yydgoto[] = {                         26,
   27,   28,   29,   30,  112,   31,   32,   33,   34,   35,
   40,   36,  113,   73,
};
static const YYINT yysindex[] = {                       256,
    0,    0, -273,    0,    0,  -65,  256, -240, -264,  567,
  567,  567, -247, -247, -247, -247, -247, -247, -247, -247,
 -247,  256,  567,  601,  601,    0,    0, -198, -214,    0,
    0,    0,    0,    0,    0,  256,  567,  567, -260, -242,
 -252, -270, -226, -215, -204,    0,    0,  567,    0,    0,
    0,    0,    0,    0,    0,    0,    0, -227, -211, -298,
 -243,  601,  601,  601,  601, -247, -247, -247, -247, -247,
    0,    0,  328,    0,  256,  256,  256,  256,  256,  256,
  256, -235,  292,  363,  397,  431,    0,    0,  567,  601,
  601,  601,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0, -196,
 -118,    0, -266,    0,    0, -205,  465, -195,  499, -190,
  533, -208,    0,    0,    0,  256, -197, -271, -188,    0,
    0,  256,  256,  256,  256,  256,  256,    0,    0,  256,
  256,  256,    0,    0,    0,    0,    0,    0,    0,    0,
    0,
};
static const YYINT yyrindex[] = {                         0,
    0,    0,    1,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,  210,   56,    0,
    0,    0,    0,    0,    0,   94,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,  109,
  162,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,
};
static const YYINT yygindex[] = {                         0,
   -3,   -2,   33,   -4,  -18,    0,    0,    0,    0,   15,
   -7,    0,    0,  -27,
};
#define YYTABLESIZE 891
static const YYINT yytable[] = {                        130,
   64,   39,   41,  141,   90,   91,   46,   47,   48,    1,
    2,    3,    4,   43,   44,   45,   37,   58,   42,   59,
   81,   60,   61,   82,   83,   75,   76,   77,   78,   79,
   80,   71,   84,   72,   74,  111,   75,   76,   77,   78,
   79,   80,   22,   85,   87,   49,   50,   51,   52,   53,
   54,   55,   56,   57,   86,   52,  117,  119,  121,   93,
   94,   95,   96,   88,   89,   92,  111,  126,  132,  103,
  104,  105,  106,  107,  108,  109,  110,  140,  134,  114,
   72,   72,   72,  136,  138,  122,  142,  123,  124,  125,
   66,   67,   68,    1,  131,   69,   70,  115,   97,   98,
   99,  100,  101,  129,   62,   63,  128,    0,   43,   64,
   65,    0,    0,  103,    0,  103,    0,  103,    0,    0,
    0,  139,    0,    0,    0,    0,    0,  143,  144,  145,
  146,  147,  148,    0,    0,  149,  150,  151,    1,    2,
    3,    4,    5,    6,    0,    0,    0,    0,    7,    8,
    9,   10,   11,   12,   38,    0,    0,    0,  127,    0,
    0,   48,   13,   14,   15,   16,   17,   18,   19,   20,
   21,   22,    0,   23,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,   24,    0,    0,    0,    0,
   25,    1,    2,    3,    4,    5,    6,    0,    0,    0,
    0,    7,    8,    9,   10,   11,   12,   38,    0,   44,
    0,    0,    0,    0,    0,   13,   14,   15,   16,   17,
   18,   19,   20,   21,   22,    0,   23,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,   24,    0,
    0,    0,    0,   25,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   64,   64,   64,
   64,   64,   64,   64,   64,    0,   64,   64,   64,   64,
   64,   64,   64,    0,   64,   64,   64,    0,    0,    0,
    0,   64,   64,   64,   64,   64,   64,   64,   64,   64,
    0,   64,   64,   64,    0,    0,    0,   64,   64,   64,
   64,   64,   64,   64,   64,   64,   64,   64,   64,   64,
   64,   64,   52,   52,   52,   52,   52,   52,   52,   52,
    0,   52,   52,   52,   52,   52,   52,   52,    0,   52,
   52,   52,    0,    0,    0,    0,   52,   52,   52,   52,
   52,   52,   52,   52,   52,   52,   52,   52,   52,    0,
    0,    0,   52,   52,   52,   52,   52,   52,   52,   52,
    0,    0,    0,   52,   52,   43,   43,   43,   43,   43,
   43,   43,   43,    0,   43,   43,   43,   43,   43,   43,
   43,    0,   43,   43,   43,    0,    0,    0,    0,   43,
   43,   43,   43,   43,   43,   43,   43,   43,   43,   43,
   43,   43,    0,    0,    0,   43,   43,   43,   43,   43,
   43,    0,    0,    0,    0,    0,    0,   43,   48,   48,
   48,   48,   48,   48,   48,   48,    0,   48,   48,   48,
   48,   48,   48,   48,    0,   48,   48,   48,    0,    0,
    0,    0,   48,   48,   48,   48,   48,   48,   48,   48,
   48,   48,   48,   48,   48,    0,    0,    0,   48,   48,
   48,   48,   48,   48,    0,   48,   44,   44,   44,   44,
   44,   44,   44,   44,    0,   44,   44,   44,   44,   44,
   44,   44,    0,   44,   44,   44,    0,    0,    0,    0,
   44,   44,   44,   44,   44,   44,   44,   44,   44,   44,
   44,   44,   44,    0,    0,    0,   44,   44,   44,   44,
   44,   44,    1,    2,    3,    4,    5,    6,    0,    0,
    0,    0,    7,    8,    9,   10,   11,   12,    0,    0,
    0,    0,    0,    0,    0,    0,   13,   14,   15,   16,
   17,   18,   19,   20,   21,   22,    0,   23,    1,    2,
    3,    4,    5,    0,    0,    0,    0,    0,    0,   24,
    0,   10,   11,   12,   25,    0,    0,    0,    0,    0,
    0,    0,   13,   14,   15,   16,   17,   18,   19,   20,
   21,   22,    0,   23,    1,    2,    3,    4,    0,    0,
    0,    0,    0,    0,    0,   24,    0,    0,    0,    0,
   25,    0,    0,    0,    0,    0,    0,    0,   13,   14,
   15,   16,   17,   18,   19,   20,   21,   22,  102,    1,
    2,    3,    4,  116,    0,    0,    0,    0,    0,    0,
    0,   24,    0,    0,    0,    0,   25,    0,    0,    0,
    0,    0,    0,   13,   14,   15,   16,   17,   18,   19,
   20,   21,   22,    1,    2,    3,    4,  118,    0,    0,
    0,    0,    0,    0,    0,    0,   24,    0,    0,    0,
    0,   25,    0,    0,    0,    0,    0,   13,   14,   15,
   16,   17,   18,   19,   20,   21,   22,    1,    2,    3,
    4,  120,    0,    0,    0,    0,    0,    0,    0,    0,
   24,    0,    0,    0,    0,   25,    0,    0,    0,    0,
    0,   13,   14,   15,   16,   17,   18,   19,   20,   21,
   22,    1,    2,    3,    4,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   24,    0,    0,    0,  133,   25,
    0,    0,    0,    0,    0,   13,   14,   15,   16,   17,
   18,   19,   20,   21,   22,    1,    2,    3,    4,    0,
    0,    0,    0,    0,    0,    0,    0,    0,   24,    0,
    0,    0,  135,   25,    0,    0,    0,    0,    0,   13,
   14,   15,   16,   17,   18,   19,   20,   21,   22,    1,
    2,    3,    4,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   24,    0,    0,    0,  137,   25,    0,    0,
    0,    0,    0,   13,   14,   15,   16,   17,   18,   19,
   20,   21,   22,    1,    2,    3,    4,    0,    0,    0,
    0,    0,    0,    0,    0,    0,   24,    0,    0,    0,
    0,   25,    0,    0,    0,    0,    0,   13,   14,   15,
   16,   17,   18,   19,   20,   21,   22,    1,    2,    3,
    4,    0,    0,    0,    0,    0,    0,    0,    0,    0,
   24,    0,    0,    0,    0,   25,    0,    0,    0,    0,
    0,   13,   14,   15,   16,   17,   18,   19,   20,   21,
   22,
};
static const YYINT yycheck[] = {                        266,
    0,    6,    7,  275,  303,  304,   10,   11,   12,  257,
  258,  259,  260,  278,  279,  280,  290,   22,  259,   23,
  263,   24,   25,  276,  295,  297,  298,  299,  300,  301,
  302,   36,  259,   37,   38,  302,  297,  298,  299,  300,
  301,  302,  290,  259,   48,   13,   14,   15,   16,   17,
   18,   19,   20,   21,  259,    0,   84,   85,   86,   62,
   63,   64,   65,  291,  276,  309,  302,  264,  274,   73,
   75,   76,   77,   78,   79,   80,   81,  275,  274,   83,
   84,   85,   86,  274,  293,   89,  275,   90,   91,   92,
  305,  306,  307,    0,  113,  310,  311,   83,   66,   67,
   68,   69,   70,  111,  303,  304,  111,   -1,    0,  308,
  309,   -1,   -1,  117,   -1,  119,   -1,  121,   -1,   -1,
   -1,  126,   -1,   -1,   -1,   -1,   -1,  132,  133,  134,
  135,  136,  137,   -1,   -1,  140,  141,  142,  257,  258,
  259,  260,  261,  262,   -1,   -1,   -1,   -1,  267,  268,
  269,  270,  271,  272,  273,   -1,   -1,   -1,  277,   -1,
   -1,    0,  281,  282,  283,  284,  285,  286,  287,  288,
  289,  290,   -1,  292,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  304,   -1,   -1,   -1,   -1,
  309,  257,  258,  259,  260,  261,  262,   -1,   -1,   -1,
   -1,  267,  268,  269,  270,  271,  272,  273,   -1,    0,
   -1,   -1,   -1,   -1,   -1,  281,  282,  283,  284,  285,
  286,  287,  288,  289,  290,   -1,  292,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  304,   -1,
   -1,   -1,   -1,  309,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,  257,  258,  259,
  260,  261,  262,  263,  264,   -1,  266,  267,  268,  269,
  270,  271,  272,   -1,  274,  275,  276,   -1,   -1,   -1,
   -1,  281,  282,  283,  284,  285,  286,  287,  288,  289,
   -1,  291,  292,  293,   -1,   -1,   -1,  297,  298,  299,
  300,  301,  302,  303,  304,  305,  306,  307,  308,  309,
  310,  311,  257,  258,  259,  260,  261,  262,  263,  264,
   -1,  266,  267,  268,  269,  270,  271,  272,   -1,  274,
  275,  276,   -1,   -1,   -1,   -1,  281,  282,  283,  284,
  285,  286,  287,  288,  289,  290,  291,  292,  293,   -1,
   -1,   -1,  297,  298,  299,  300,  301,  302,  303,  304,
   -1,   -1,   -1,  308,  309,  257,  258,  259,  260,  261,
  262,  263,  264,   -1,  266,  267,  268,  269,  270,  271,
  272,   -1,  274,  275,  276,   -1,   -1,   -1,   -1,  281,
  282,  283,  284,  285,  286,  287,  288,  289,  290,  291,
  292,  293,   -1,   -1,   -1,  297,  298,  299,  300,  301,
  302,   -1,   -1,   -1,   -1,   -1,   -1,  309,  257,  258,
  259,  260,  261,  262,  263,  264,   -1,  266,  267,  268,
  269,  270,  271,  272,   -1,  274,  275,  276,   -1,   -1,
   -1,   -1,  281,  282,  283,  284,  285,  286,  287,  288,
  289,  290,  291,  292,  293,   -1,   -1,   -1,  297,  298,
  299,  300,  301,  302,   -1,  304,  257,  258,  259,  260,
  261,  262,  263,  264,   -1,  266,  267,  268,  269,  270,
  271,  272,   -1,  274,  275,  276,   -1,   -1,   -1,   -1,
  281,  282,  283,  284,  285,  286,  287,  288,  289,  290,
  291,  292,  293,   -1,   -1,   -1,  297,  298,  299,  300,
  301,  302,  257,  258,  259,  260,  261,  262,   -1,   -1,
   -1,   -1,  267,  268,  269,  270,  271,  272,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  281,  282,  283,  284,
  285,  286,  287,  288,  289,  290,   -1,  292,  257,  258,
  259,  260,  261,   -1,   -1,   -1,   -1,   -1,   -1,  304,
   -1,  270,  271,  272,  309,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,  281,  282,  283,  284,  285,  286,  287,  288,
  289,  290,   -1,  292,  257,  258,  259,  260,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,  304,   -1,   -1,   -1,   -1,
  309,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  281,  282,
  283,  284,  285,  286,  287,  288,  289,  290,  291,  257,
  258,  259,  260,  261,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,  304,   -1,   -1,   -1,   -1,  309,   -1,   -1,   -1,
   -1,   -1,   -1,  281,  282,  283,  284,  285,  286,  287,
  288,  289,  290,  257,  258,  259,  260,  261,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  304,   -1,   -1,   -1,
   -1,  309,   -1,   -1,   -1,   -1,   -1,  281,  282,  283,
  284,  285,  286,  287,  288,  289,  290,  257,  258,  259,
  260,  261,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
  304,   -1,   -1,   -1,   -1,  309,   -1,   -1,   -1,   -1,
   -1,  281,  282,  283,  284,  285,  286,  287,  288,  289,
  290,  257,  258,  259,  260,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,  304,   -1,   -1,   -1,  274,  309,
   -1,   -1,   -1,   -1,   -1,  281,  282,  283,  284,  285,
  286,  287,  288,  289,  290,  257,  258,  259,  260,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  304,   -1,
   -1,   -1,  274,  309,   -1,   -1,   -1,   -1,   -1,  281,
  282,  283,  284,  285,  286,  287,  288,  289,  290,  257,
  258,  259,  260,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,  304,   -1,   -1,   -1,  274,  309,   -1,   -1,
   -1,   -1,   -1,  281,  282,  283,  284,  285,  286,  287,
  288,  289,  290,  257,  258,  259,  260,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  304,   -1,   -1,   -1,
   -1,  309,   -1,   -1,   -1,   -1,   -1,  281,  282,  283,
  284,  285,  286,  287,  288,  289,  290,  257,  258,  259,
  260,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
  304,   -1,   -1,   -1,   -1,  309,   -1,   -1,   -1,   -1,
   -1,  281,  282,  283,  284,  285,  286,  287,  288,  289,
  290,
};
#define YYFINAL 26
#ifndef YYDEBUG
#define YYDEBUG 0
#endif
#define YYMAXTOKEN 311
#define YYUNDFTOKEN 328
#define YYTRANSLATE(a) ((a) > YYMAXTOKEN ? YYUNDFTOKEN : (a))
#if YYDEBUG
static const char *const yyname[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"INTEGER_LITERAL","FLOAT_LITERAL",
"ID","BOOLEAN_LITERAL","NIL","IF","THEN","ELSE","DO","END","CASE","LET","DEF",
"HEAD","TAIL","PAIR","NUL","CIFR","MATCH","COLON","ALL","INT","FLOAT","BOOL",
"TOINT","TOFLOAT","IN","INB","INH","OUT","OUTB","OUTH","HALT","LPAR","RPAR",
"LBRAC","RBRAC","UMINUS","INIT","UPLUS","EQ","NOTEQ","LT","GT","AND","OR",
"PLUS","MINUS","TIMES","DIVIDE","MODULO","FPLUS","FMINUS","FTIMES","FDIVIDE",0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"illegal-symbol",
};
static const char *const yyrule[] = {
"$accept : program",
"program : statement_list",
"statement : function_definition",
"statement : expression_definition",
"statement : if_conditional",
"statement : pattern_matching",
"statement : list_ops",
"statement : expression",
"statement_list : statement",
"statement_list : statement_list statement",
"function_definition : DEF INT ID expression_list CIFR statement",
"function_definition : DEF FLOAT ID expression_list CIFR statement",
"function_definition : DEF BOOL ID expression_list CIFR statement",
"function_definition : DEF INT ID NIL CIFR statement",
"function_definition : DEF FLOAT ID NIL CIFR statement",
"function_definition : DEF BOOL ID NIL CIFR statement",
"expression_definition : LET ID INIT expression",
"expression_definition : LET ID INIT list_ops",
"if_conditional : IF conditional_expression THEN statement ELSE statement",
"expression_list : expression_list expression",
"expression_list : expression",
"conditional_expression : statement GT statement",
"conditional_expression : statement LT statement",
"conditional_expression : statement EQ statement",
"conditional_expression : statement NOTEQ statement",
"conditional_expression : statement OR statement",
"conditional_expression : statement AND statement",
"conditional_expression : NUL expression",
"pattern_matching : CASE statement COLON match_expression_list END",
"match_expression_list : match_expression_list match_expression",
"match_expression_list : match_expression",
"match_expression : OR statement MATCH statement",
"match_expression : OR conditional_expression MATCH statement",
"match_expression : OR ALL MATCH statement",
"list_ops : PAIR expression expression",
"list_ops : LBRAC expression COLON expression RBRAC",
"list_ops : HEAD expression",
"list_ops : TAIL expression",
"list_ops : NIL",
"expression : MINUS term PLUS term",
"expression : MINUS term MINUS term",
"expression : term MINUS term",
"expression : term PLUS term",
"expression : MINUS term",
"expression : term",
"expression : term FPLUS term",
"expression : FMINUS term FMINUS term",
"expression : term FMINUS term",
"expression : FMINUS term",
"term : factor TIMES factor",
"term : factor DIVIDE factor",
"term : factor MODULO factor",
"term : factor",
"term : factor FTIMES factor",
"term : factor FDIVIDE factor",
"term : TOFLOAT factor",
"term : TOINT factor",
"term : IN factor",
"term : INB factor",
"term : INH factor",
"term : OUT factor",
"term : OUTB factor",
"term : OUTH factor",
"term : HALT factor",
"factor : ID",
"factor : INTEGER_LITERAL",
"factor : FLOAT_LITERAL",
"factor : BOOLEAN_LITERAL",
"factor : ID LPAR expression_list RPAR",
"factor : LPAR statement RPAR",

};
#endif

int      yydebug;
int      yynerrs;

int      yyerrflag;
int      yychar;
YYSTYPE  yyval;
YYSTYPE  yylval;

/* define the initial stack-sizes */
#ifdef YYSTACKSIZE
#undef YYMAXDEPTH
#define YYMAXDEPTH  YYSTACKSIZE
#else
#ifdef YYMAXDEPTH
#define YYSTACKSIZE YYMAXDEPTH
#else
#define YYSTACKSIZE 10000
#define YYMAXDEPTH  10000
#endif
#endif

#define YYINITSTACKSIZE 200

typedef struct {
    unsigned stacksize;
    YYINT    *s_base;
    YYINT    *s_mark;
    YYINT    *s_last;
    YYSTYPE  *l_base;
    YYSTYPE  *l_mark;
} YYSTACKDATA;
/* variables for the parser stack */
static YYSTACKDATA yystack;
#line 193 "yasl.y"


void yyerror(tree **ptree, hashmap **sm, const char *s)
{

  extern int yylineno;	// defined and maintained in lex.c
  extern char *yytext;	// defined and maintained in lex.c
	
  printf("%s", s);
  printf(" at symbol \"");
  printf("%s", yytext);
  printf("\" on line ");
  printf("%d\n", yylineno);
  exit(1);
 }

int yywrap() {
	return(1);
 }



#line 559 "yasl.tab.c"

#if YYDEBUG
#include <stdio.h>		/* needed for printf */
#endif

#include <stdlib.h>	/* needed for malloc, etc */
#include <string.h>	/* needed for memset */

/* allocate initial stack or double stack size, up to YYMAXDEPTH */
static int yygrowstack(YYSTACKDATA *data)
{
    int i;
    unsigned newsize;
    YYINT *newss;
    YYSTYPE *newvs;

    if ((newsize = data->stacksize) == 0)
        newsize = YYINITSTACKSIZE;
    else if (newsize >= YYMAXDEPTH)
        return YYENOMEM;
    else if ((newsize *= 2) > YYMAXDEPTH)
        newsize = YYMAXDEPTH;

    i = (int) (data->s_mark - data->s_base);
    newss = (YYINT *)realloc(data->s_base, newsize * sizeof(*newss));
    if (newss == 0)
        return YYENOMEM;

    data->s_base = newss;
    data->s_mark = newss + i;

    newvs = (YYSTYPE *)realloc(data->l_base, newsize * sizeof(*newvs));
    if (newvs == 0)
        return YYENOMEM;

    data->l_base = newvs;
    data->l_mark = newvs + i;

    data->stacksize = newsize;
    data->s_last = data->s_base + newsize - 1;
    return 0;
}

#if YYPURE || defined(YY_NO_LEAKS)
static void yyfreestack(YYSTACKDATA *data)
{
    free(data->s_base);
    free(data->l_base);
    memset(data, 0, sizeof(*data));
}
#else
#define yyfreestack(data) /* nothing */
#endif

#define YYABORT  goto yyabort
#define YYREJECT goto yyabort
#define YYACCEPT goto yyaccept
#define YYERROR  goto yyerrlab

int
YYPARSE_DECL()
{
    int yym, yyn, yystate;
#if YYDEBUG
    const char *yys;

    if ((yys = getenv("YYDEBUG")) != 0)
    {
        yyn = *yys;
        if (yyn >= '0' && yyn <= '9')
            yydebug = yyn - '0';
    }
#endif

    yynerrs = 0;
    yyerrflag = 0;
    yychar = YYEMPTY;
    yystate = 0;

#if YYPURE
    memset(&yystack, 0, sizeof(yystack));
#endif

    if (yystack.s_base == NULL && yygrowstack(&yystack) == YYENOMEM) goto yyoverflow;
    yystack.s_mark = yystack.s_base;
    yystack.l_mark = yystack.l_base;
    yystate = 0;
    *yystack.s_mark = 0;

yyloop:
    if ((yyn = yydefred[yystate]) != 0) goto yyreduce;
    if (yychar < 0)
    {
        if ((yychar = YYLEX) < 0) yychar = YYEOF;
#if YYDEBUG
        if (yydebug)
        {
            yys = yyname[YYTRANSLATE(yychar)];
            printf("%sdebug: state %d, reading %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
    }
    if ((yyn = yysindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: state %d, shifting to state %d\n",
                    YYPREFIX, yystate, yytable[yyn]);
#endif
        if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
        {
            goto yyoverflow;
        }
        yystate = yytable[yyn];
        *++yystack.s_mark = yytable[yyn];
        *++yystack.l_mark = yylval;
        yychar = YYEMPTY;
        if (yyerrflag > 0)  --yyerrflag;
        goto yyloop;
    }
    if ((yyn = yyrindex[yystate]) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
    {
        yyn = yytable[yyn];
        goto yyreduce;
    }
    if (yyerrflag) goto yyinrecovery;

    YYERROR_CALL("syntax error");

    goto yyerrlab;

yyerrlab:
    ++yynerrs;

yyinrecovery:
    if (yyerrflag < 3)
    {
        yyerrflag = 3;
        for (;;)
        {
            if ((yyn = yysindex[*yystack.s_mark]) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: state %d, error recovery shifting\
 to state %d\n", YYPREFIX, *yystack.s_mark, yytable[yyn]);
#endif
                if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
                {
                    goto yyoverflow;
                }
                yystate = yytable[yyn];
                *++yystack.s_mark = yytable[yyn];
                *++yystack.l_mark = yylval;
                goto yyloop;
            }
            else
            {
#if YYDEBUG
                if (yydebug)
                    printf("%sdebug: error recovery discarding state %d\n",
                            YYPREFIX, *yystack.s_mark);
#endif
                if (yystack.s_mark <= yystack.s_base) goto yyabort;
                --yystack.s_mark;
                --yystack.l_mark;
            }
        }
    }
    else
    {
        if (yychar == YYEOF) goto yyabort;
#if YYDEBUG
        if (yydebug)
        {
            yys = yyname[YYTRANSLATE(yychar)];
            printf("%sdebug: state %d, error recovery discards token %d (%s)\n",
                    YYPREFIX, yystate, yychar, yys);
        }
#endif
        yychar = YYEMPTY;
        goto yyloop;
    }

yyreduce:
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: state %d, reducing by rule %d (%s)\n",
                YYPREFIX, yystate, yyn, yyrule[yyn]);
#endif
    yym = yylen[yyn];
    if (yym)
        yyval = yystack.l_mark[1-yym];
    else
        memset(&yyval, 0, sizeof yyval);
    switch (yyn)
    {
case 1:
#line 72 "yasl.y"
	{*ptree=yystack.l_mark[0].a_tree;}
break;
case 8:
#line 86 "yasl.y"
	{yyval.a_tree = yystack.l_mark[0].a_tree;}
break;
case 9:
#line 87 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-1].a_tree,";;", yystack.l_mark[0].a_tree);}
break;
case 10:
#line 92 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[-3].id, "func"); tree *func = make_operator(yystack.l_mark[-2].a_tree,yystack.l_mark[-3].id,yystack.l_mark[0].a_tree); func->s_type = t_int; yyval.a_tree = func;}
break;
case 11:
#line 93 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[-3].id, "func"); tree *func = make_operator(yystack.l_mark[-2].a_tree,yystack.l_mark[-3].id,yystack.l_mark[0].a_tree); func->s_type = t_float; yyval.a_tree = func;}
break;
case 12:
#line 94 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[-3].id, "func"); tree *func = make_operator(yystack.l_mark[-2].a_tree,yystack.l_mark[-3].id,yystack.l_mark[0].a_tree); func->s_type = t_bool; yyval.a_tree = func;}
break;
case 13:
#line 95 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[-3].id, "func"); tree *func = make_operator(yystack.l_mark[-2].a_tree,yystack.l_mark[-3].id,yystack.l_mark[0].a_tree); func->s_type = t_int; yyval.a_tree = func;}
break;
case 14:
#line 96 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[-3].id, "func"); tree *func = make_operator(yystack.l_mark[-2].a_tree,yystack.l_mark[-3].id,yystack.l_mark[0].a_tree); func->s_type = t_float; yyval.a_tree = func;}
break;
case 15:
#line 97 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[-3].id, "func"); tree *func = make_operator(yystack.l_mark[-2].a_tree,yystack.l_mark[-3].id,yystack.l_mark[0].a_tree); func->s_type = t_bool; yyval.a_tree = func;}
break;
case 16:
#line 102 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[-2].id, "var"); yyval.a_tree = make_alias(yystack.l_mark[-2].id,yystack.l_mark[0].a_tree);}
break;
case 17:
#line 103 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[-2].id, "list"); yyval.a_tree = make_alias(yystack.l_mark[-2].id,yystack.l_mark[0].a_tree);}
break;
case 18:
#line 107 "yasl.y"
	{yyval.a_tree = make_if_else(yystack.l_mark[-4].a_tree, yystack.l_mark[-2].a_tree, yystack.l_mark[0].a_tree);}
break;
case 19:
#line 112 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-1].a_tree,";", yystack.l_mark[0].a_tree);}
break;
case 20:
#line 113 "yasl.y"
	{yyval.a_tree = yystack.l_mark[0].a_tree;}
break;
case 21:
#line 117 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, ">", yystack.l_mark[0].a_tree);}
break;
case 22:
#line 118 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "<", yystack.l_mark[0].a_tree);}
break;
case 23:
#line 119 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "==", yystack.l_mark[0].a_tree);}
break;
case 24:
#line 120 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "!", yystack.l_mark[0].a_tree);}
break;
case 25:
#line 121 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "|", yystack.l_mark[0].a_tree);}
break;
case 26:
#line 122 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "&", yystack.l_mark[0].a_tree);}
break;
case 27:
#line 123 "yasl.y"
	{yyval.a_tree = make_operator(NULL, "NUL", yystack.l_mark[0].a_tree);}
break;
case 28:
#line 128 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-3].a_tree,"@", yystack.l_mark[-1].a_tree);}
break;
case 29:
#line 133 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-1].a_tree,"@@", yystack.l_mark[0].a_tree);}
break;
case 30:
#line 134 "yasl.y"
	{yyval.a_tree = yystack.l_mark[0].a_tree;}
break;
case 31:
#line 138 "yasl.y"
	{yyval.a_tree = make_if(yystack.l_mark[-2].a_tree,yystack.l_mark[0].a_tree);}
break;
case 32:
#line 139 "yasl.y"
	{yyval.a_tree = make_if(yystack.l_mark[-2].a_tree,yystack.l_mark[0].a_tree);}
break;
case 33:
#line 140 "yasl.y"
	{yyval.a_tree = yystack.l_mark[0].a_tree;}
break;
case 34:
#line 144 "yasl.y"
	{yyval.a_tree = make_cons(yystack.l_mark[-1].a_tree,yystack.l_mark[0].a_tree);}
break;
case 35:
#line 145 "yasl.y"
	{yyval.a_tree = make_cons(yystack.l_mark[-3].a_tree,yystack.l_mark[-1].a_tree);}
break;
case 36:
#line 146 "yasl.y"
	{yyval.a_tree = make_operator(NULL, "HEAD", yystack.l_mark[0].a_tree);}
break;
case 37:
#line 147 "yasl.y"
	{yyval.a_tree = make_operator(NULL, "TAIL", yystack.l_mark[0].a_tree);}
break;
case 38:
#line 148 "yasl.y"
	{yyval.a_tree = make_operator(NULL, "NIL", NULL);}
break;
case 39:
#line 153 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[0].a_tree, "-", yystack.l_mark[-2].a_tree);}
break;
case 40:
#line 154 "yasl.y"
	{yyval.a_tree = make_operator(NULL, "-", make_operator (yystack.l_mark[-2].a_tree, "+", yystack.l_mark[0].a_tree));}
break;
case 41:
#line 155 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "-", yystack.l_mark[0].a_tree);}
break;
case 42:
#line 156 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "+", yystack.l_mark[0].a_tree);}
break;
case 43:
#line 157 "yasl.y"
	{yyval.a_tree = make_operator(NULL, "-", yystack.l_mark[0].a_tree);}
break;
case 44:
#line 158 "yasl.y"
	{yyval.a_tree = yystack.l_mark[0].a_tree;}
break;
case 45:
#line 159 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "+:", yystack.l_mark[0].a_tree);}
break;
case 46:
#line 160 "yasl.y"
	{yyval.a_tree = make_operator(NULL, "-:", make_operator (yystack.l_mark[-2].a_tree, "+:", yystack.l_mark[0].a_tree));}
break;
case 47:
#line 161 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "-:", yystack.l_mark[0].a_tree);}
break;
case 48:
#line 162 "yasl.y"
	{yyval.a_tree = make_operator(NULL, "-:", yystack.l_mark[0].a_tree);}
break;
case 49:
#line 166 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "*", yystack.l_mark[0].a_tree);}
break;
case 50:
#line 167 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "/", yystack.l_mark[0].a_tree);}
break;
case 51:
#line 168 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "%", yystack.l_mark[0].a_tree);}
break;
case 52:
#line 169 "yasl.y"
	{yyval.a_tree = yystack.l_mark[0].a_tree;}
break;
case 53:
#line 170 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "*:", yystack.l_mark[0].a_tree);}
break;
case 54:
#line 171 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-2].a_tree, "/:", yystack.l_mark[0].a_tree);}
break;
case 55:
#line 172 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"FLT_",yystack.l_mark[0].a_tree); yyval.a_tree->s_type = t_float;}
break;
case 56:
#line 173 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"INT_",yystack.l_mark[0].a_tree);yyval.a_tree->s_type = t_int;}
break;
case 57:
#line 174 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"IN",yystack.l_mark[0].a_tree);}
break;
case 58:
#line 175 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"INB",yystack.l_mark[0].a_tree);}
break;
case 59:
#line 176 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"INH",yystack.l_mark[0].a_tree);}
break;
case 60:
#line 177 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"OUT",yystack.l_mark[0].a_tree);}
break;
case 61:
#line 178 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"OUTB",yystack.l_mark[0].a_tree);}
break;
case 62:
#line 179 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"OUTH",yystack.l_mark[0].a_tree);}
break;
case 63:
#line 180 "yasl.y"
	{yyval.a_tree = make_operator(NULL,"HALT",yystack.l_mark[0].a_tree);}
break;
case 64:
#line 184 "yasl.y"
	{sym_hashmap_put(*sym, yystack.l_mark[0].id, "var"); yyval.a_tree = make_variable(yystack.l_mark[0].id);}
break;
case 65:
#line 185 "yasl.y"
	{yyval.a_tree = make_int(yystack.l_mark[0].int_val);}
break;
case 66:
#line 186 "yasl.y"
	{yyval.a_tree = make_real(yystack.l_mark[0].real_val);}
break;
case 67:
#line 187 "yasl.y"
	{yyval.a_tree = make_bool(yystack.l_mark[0].bool_val);}
break;
case 68:
#line 188 "yasl.y"
	{yyval.a_tree = make_operator(yystack.l_mark[-1].a_tree, yystack.l_mark[-3].id, NULL);}
break;
case 69:
#line 189 "yasl.y"
	{yyval.a_tree = yystack.l_mark[-1].a_tree;}
break;
#line 1013 "yasl.tab.c"
    }
    yystack.s_mark -= yym;
    yystate = *yystack.s_mark;
    yystack.l_mark -= yym;
    yym = yylhs[yyn];
    if (yystate == 0 && yym == 0)
    {
#if YYDEBUG
        if (yydebug)
            printf("%sdebug: after reduction, shifting from state 0 to\
 state %d\n", YYPREFIX, YYFINAL);
#endif
        yystate = YYFINAL;
        *++yystack.s_mark = YYFINAL;
        *++yystack.l_mark = yyval;
        if (yychar < 0)
        {
            if ((yychar = YYLEX) < 0) yychar = YYEOF;
#if YYDEBUG
            if (yydebug)
            {
                yys = yyname[YYTRANSLATE(yychar)];
                printf("%sdebug: state %d, reading %d (%s)\n",
                        YYPREFIX, YYFINAL, yychar, yys);
            }
#endif
        }
        if (yychar == YYEOF) goto yyaccept;
        goto yyloop;
    }
    if ((yyn = yygindex[yym]) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn];
    else
        yystate = yydgoto[yym];
#if YYDEBUG
    if (yydebug)
        printf("%sdebug: after reduction, shifting from state %d \
to state %d\n", YYPREFIX, *yystack.s_mark, yystate);
#endif
    if (yystack.s_mark >= yystack.s_last && yygrowstack(&yystack) == YYENOMEM)
    {
        goto yyoverflow;
    }
    *++yystack.s_mark = (YYINT) yystate;
    *++yystack.l_mark = yyval;
    goto yyloop;

yyoverflow:
    YYERROR_CALL("yacc stack overflow");

yyabort:
    yyfreestack(&yystack);
    return (1);

yyaccept:
    yyfreestack(&yystack);
    return (0);
}
