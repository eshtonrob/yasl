#!/bin/sh
#building yasl
#files used in codegen	yasl.y, yasl.lex
#
#call functions with 'source build.sh; build' for eg.
#or 		     ' . build.sh; build'
#or	 	     ' . build.sh && build'
#
#after the initial sourcing simply call the function needed directly
 

	build(){
	byacc -d -b yasl yasl.y
	flex -o yasl.lex.c yasl.lex
	gcc -o yaslc hashmap/src/hashmap.c sds/sds.c yasl.lex.c yasl.tab.c yasl.c -lfl -lm -Wall -Wno-unused-function
	}

        build_debug(){
	byacc -d -b yasl yasl.y
	flex -o yasl.lex.c yasl.lex
	gcc -o yaslc hashmap/src/hashmap.c sds/sds.c yasl.lex.c yasl.tab.c yasl.c -lfl -lm -g -Wall -Wno-unused-function
	}

	clean(){
	rm yaslc
	} 


	rebuild(){
	gcc -o yaslc hashmap/src/hashmap.c sds/sds.c yasl.lex.c yasl.tab.c yasl.c -lfl -lm -Wall -Wno-unused-function
	}

	rebuild_debug(){
	gcc -o yaslc hashmap/src/hashmap.c sds/sds.c yasl.lex.c yasl.tab.c yasl.c -lfl -lm -g -Wall -Wno-unused-function	
	}
