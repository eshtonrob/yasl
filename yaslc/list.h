#ifndef LIST_H
#define LIST_H

#include <stdlib.h>
#include <string.h>
#include "util.h"

 typedef struct node{
    char *data;
    struct node *next;
 }node;

 
 static node *list(char *t3){/*Create a new node for the list*/
 node *result = (node*) malloc(sizeof(node));
 result->next = NULL;
 int num_chars = strlen(t3)+1;
 result->data = malloc(num_chars);
 ysl_strlcpy(result->data,t3,num_chars);
 return result;
 }

 static node *prepend(node *head, node *nhd){ /*prepend node nhd to list head*/
 node *result = (node*) malloc(sizeof(node));
 result = nhd;
 result->next = head;
 return result;
 }

 static node *reverse(node *list) { 
    node *prev = NULL;
    node *next;

    while (list) {
        next = list->next;
        list->next = prev;
        prev = list;
        list = next;
    }
    return prev;
 }


 static node *copy(node *l){ /*copy a list*/
 if (l == NULL) return NULL;
 node *result = (node*) malloc(sizeof(node));
 
 int num_chars = strlen(l->data)+1;
 result->data = malloc(num_chars);
 ysl_strlcpy(result->data, l->data, num_chars);

 result->next = copy(l->next);
 return result;
 }


#endif
