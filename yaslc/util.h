#ifndef UTIL_H
#define UTIL_H

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define cns 900 //cons
#define fi  539 //if
#define els 755 //else
#define lat 372 //let
#define hed 14  //head
#define tal 312 //tail
#define nvl 162 //nul
#define nli 138 //nil
#define flt 905 //float
#define tni 953 //int  
#define pls 183 //plus
#define mns 16	//minus
#define div 369 //divide
#define tim 453 //times
#define fpls 143 //fplus
#define fmns 777 //fminus
#define fdiv 599 //fdivide
#define ftim 73 //ftimes
#define at  989	//match
#define dat 622 //match list
#define stl 137 //statement list
#define exl 894 //expression list
#define gtd 301 //greater than
#define lsd 846 //less than
#define nte 399	//not
#define lor 915 //or
#define lan 57  //and
#define leq 529 //equal to
#define mod 684 //modulus
#define def 635 //def keyword
#define lpr 726 //left parentheses (
#define rpr 10  //right parentheses )
#define ni  295 //IN	
#define nih 554 //INH
#define nib 430 //INB
#define tuo 662 //OUT
#define tuoh 371 //OUTH
#define tuob 830 //OUTB
#define tlah 603 //HALT

 /*One at a time simple hash function, the Jenkins Hash*/
 static uint32_t oat_hash(const char *key/*int len*/){ 

    //unsigned char *p = key;
    uint32_t h = 0;
    //int i;
    //int len = 8;  

    for(; *key; ++key)//for (i = 0; i < len; i++)
    {
        h += *key; //h += p[i];
        h += (h << 10);
        h ^= (h >> 6);
    }

    h += (h << 3);
    h ^= (h >> 11);
    h += (h << 15);

    return h; //should return int
 }

  /* From the BSD kernel, ysl_strlcpy is the default behaviour:
  * Copy src to string dst of size siz.  At most siz-1 characters
  * will be copied.  Always NUL terminates (unless siz == 0).
  * Returns strlen(src); if retval >= siz, truncation occurred.
  *
  *      $OpenBSD: ysl_strlcpy.c,v 1.11 2006/05/05 15:27:38 millert Exp $ 
 
  *-
  * Copyright (c) 1998 Todd C. Miller <Todd.Miller@courtesan.com>
  *
  * Permission to use, copy, modify, and distribute this software for any
  * purpose with or without fee is hereby granted, provided that the above
  * copyright notice and this permission notice appear in all copies.
  *
  * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  */
 static size_t ysl_strlcpy(char * __restrict dst, const char * __restrict src, size_t siz){
         
          char *d = dst;
         const char *s = src;
         size_t n = siz;
 
         /* Copy as many bytes as will fit */
         if (n != 0) {
                 while (--n != 0) {
                         if ((*d++ = *s++) == '\0')
                                 break;
                 }
         }
 
         /* Not enough room in dst, add NUL and traverse rest of src */
         if (n == 0) {
                 if (siz != 0)
                         *d = '\0';              /* NUL-terminate dst */
                 while (*s++)
                         ;
         }
 
         return(s - src - 1);    /* count does not include NUL */
 }

 
 static char *btos(bool b){ //small function that returns true or false
   
   if(b){return "true";}
   return "false";

 }

 static char *ftos(double f){ //float to string conversion

 char *buffer = malloc(64);
 snprintf(buffer, 64, "%a",f); // use hex strings for floating point 
 return buffer;

 }

 static void frepl(char *source, char *old, char *nuw){ //replace substring

    while (*source) {
         if(source == old)
        {
            source = nuw;
        }

        ++source;
   }
 }


 static int ysl_wc(const char* txt){ //word count 

  int counted = 0; // result
 
    // state:
    const char* it = txt;
    int inword = 0;
 
    do switch(*it) {
        case '\0': 
        case ' ': 
	case '\t': 
	case '\n': 
	case '\r': 
	case '\v':
	case '\f': if (inword) { inword = 0; counted++; }
                   break;
        default:   inword = 1;

    } while(*it++);
 
    return counted;

 }


 static bool isoperator(const char *ch){ //check if string is known math,logical operator or symbol
  
 bool isop = false;

 int optr = oat_hash(ch) % 1000;

    switch(optr){
     case pls:  /*fallthrough*/
     case mns:
     case tim:
     case mod: 
     case div:  

     case fpls:  /*fallthrough*/
     case fmns:
     case ftim:
     case fdiv: 
     
     /*logical operators*/
     case gtd:   /*fallthrough*/
     case lsd:
     case lor:
     case nte:
     case lan:
     case leq: isop = true; 
     
    }
    
   return isop;
 }



 static bool isnumeric (const char *ch) //check if string is a valid number
 {
    if (ch == NULL || *ch == '\0' || isspace(*ch))
      return false;

    char *p;
    strtod (ch, &p);

    return (bool)(*p == '\0');
 }




 static char *ysl_frev(const char *txt){   //reverse a string

    int len = strlen(txt);
    int i = 0;
    int j = len - 1;
    len++;

    char *revstr = (char*)malloc(len);
    strncpy(revstr, txt, len);
    

    while(i <= j) {
        char temp = *(revstr + i);
        *(revstr + i) = *(revstr + j);
        *(revstr + j) = temp;
        i++;
        j--;
    }
  
  return revstr;

 }


 static int ysl_cmp(const void *str1, const void *str2){ //string compare for qsort

    const char **ia = (const char **)str1;
    const char **ib = (const char **)str2;
    return strcmp(*ia, *ib);
 }

#endif


/*All the necessary optr codes were generated with the following code
* on ideone.com
*
* #include <stdio.h>
* #include <stdint.h>
* #include <string.h>
*
* static uint32_t oat_hash(const char *key){ 
*
*    //unsigned char *p = key;
*    uint32_t h = 0;
*    //int i;
*    //int len = 8;  
*
*    for(; *key; ++key)//for (i = 0; i < len; i++)
*    {
*        h += *key; //h += p[i];
*        h += (h << 10);
*        h ^= (h >> 6);
*    }
*
*    h += (h << 3);
*    h ^= (h >> 11);
*    h += (h << 15);
*
*    return h; //should return int
* }
* 
*
* int main(int argc, char** argv) {
*	char id[5]; uint32_t h;
*	scanf("%s",id);
*
*	h = oat_hash(id);
*	int h2 = h % 1000;
*	
*	printf("%s is \n",id);
*	printf("%i\n",h);
*	printf("%i\n",h2);
*		
*	
*	return 0;
* }
*
*Below are the outputs from the hash in both original and modded forms
*(mod 1000)

HALT is 
1962006603
603

OUTB is 
-86781466
830

OUTH is 
384207371
371

OUT is 
-851510634
662

INB is 
-888429866
430

INH is 
1968371554
554

IN is 
755730295
295

) is 
-299705286
10

( is 
-608913570
726

Success	#stdin #stdout 0s 4360KB
def is 
-974664661
635

nil is 
-821610158
138

% is 
-1520481612
684

Success	#stdin #stdout 0s 4520KB
INT_ is 
-575595343
953

Success	#stdin #stdout 0s 4252KB
FLT_ is 
-953544391
905

Success	#stdin #stdout 0s 10320KB
NUL is 
-1753276134
162

Success	#stdin #stdout 0s 4556KB
TAIL is 
429996312
312

Success	#stdin #stdout 0s 9432KB
HEAD is 
-82152282
14

Success	#stdin #stdout 0s 10320KB
LET is 
2117638372
372

Success	#stdin #stdout 0s 9432KB
ELSE is 
1252164755
755

Success	#stdin #stdout 0s 4592KB
IF is 
-1170824757
539

Success	#stdin #stdout 0s 10320KB
CONS is 
2062265900
900

Success	#stdin #stdout 0s 4432KB
== is 
-1151094767
529

Success	#stdin #stdout 0s 10320KB
& is 
-1233982245
51

Success	#stdin #stdout 0s 9432KB
& is 
-1233982245
51

Success	#stdin #stdout 0s 4588KB
| is 
-606390381
915

Success	#stdin #stdout 0s 10320KB
! is 
1536964399
399

Success	#stdin #stdout 0s 9432KB
< is 
1232048846
846

Success	#stdin #stdout 0s 10320KB
> is 
1844665301
301

Success	#stdin #stdout 0s 4536KB
; is 
897542894
894

Success	#stdin #stdout 0s 4408KB
;; is 
-1452831159
137

Success	#stdin #stdout 0s 4392KB
/ is 
1518810369
369

Success	#stdin #stdout 0s 4480KB
/: is 
2071347599
599

Success	#stdin #stdout 0s 4436KB
*: is 
-1208762223
73

Success	#stdin #stdout 0s 4412KB
* is 
-13074843
453

Success	#stdin #stdout 0s 9432KB
-: is 
-1445617519
777

Success	#stdin #stdout 0s 9432KB
- is 
924479016
16

Success	#stdin #stdout 0s 9432KB
@@ is 
421849622
622

Success	#stdin #stdout 0s 9432KB
@ is 
-1816156307
989

Success	#stdin #stdout 0s 4528KB
+: is 
-629341153
143

Success	#stdin #stdout 0s 4524KB
+ is 
300000183
183
*/
