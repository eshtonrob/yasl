#ifndef CODEGEN_H
#define CODEGEN_H

#include "sds/sds.h"
#include "list.h"
#include "util.h"


 /*lambda defines a complete list of s-atoms that define a lambda expression 
 * along with a string of all the parameters in the expression
 */
 typedef struct lambda{

  node *lm_list;
  char *args;

 }lambda;

 static char *get_unique_alpha(const char *alpha){
 
    int sz = strlen(alpha)+1;
    char *adup = malloc(sz);
    ysl_strlcpy(adup, alpha, sz);
 
    node *unl = NULL; sds unq1 = sdsempty();


    /*handle single parameter strings */
    char *lchr = strrchr(adup,'.');
    char *fchr = strchr(adup, '.');
    if((lchr-adup) == (fchr-adup)) {
       char *bdup;
        unq1 = sdscat(unq1, "\\");
        unq1 = sdscat(unq1, adup);
        bdup = malloc(sdslen(unq1)+1);
        ysl_strlcpy(bdup, unq1, sdslen(unq1)+1);
      return bdup;
      }/*end handle single parameter strings */

    char *unq_params;
    sds *tokens; 
    int count, j = 0;

   sds line = sdsnew(adup);
   tokens = sdssplitlen(line,sdslen(line),".",1,&count); // split on period
  
  
   qsort(tokens, count, sizeof(sds), ysl_cmp); //sort string array  qsort (x, sizeof(x)/sizeof(*x), sizeof(*x), comp); 
      
   for (int i = 0; i < count-1; i++) {      //find duplicates now
            if (strcmp(tokens[i],tokens[i+1]) != 0) {
                tokens[j++] = tokens[i];    // copy-in next unique

                unl = prepend(unl, list(tokens[i]));
            }
        }

   tokens[j++] = tokens[count-1];    // copy-in last value

   unl = prepend(unl, list(tokens[count-1]));
   while(unl){
      unq1 = sdscat(unq1, "\\");
      unq1 = sdscat(unq1, unl->data);
      unq1 = sdscat(unq1, ".");
      unl = unl->next;
     }
     
    sdstrim(unq1, ".");
    sdstrim(unq1, "\\");
    sds unq2 = sdsnew("\\");
    unq2 = sdscatsds(unq2, unq1);
   /*
   sds unq = sdsjoinsds(tokens, j, ".", 1); //the unique length is j
   sdstrim(unq, ".");
   unq = sdsnew(ysl_frev(unq)); //reverse
   unq = sdscat(unq, "."); //add trailing period
    */

   unq_params = malloc(sdslen(unq2)+1);
   ysl_strlcpy(unq_params, unq2, sdslen(unq2)+1);

   sdsfreesplitres(tokens,j);
   sdsfree(unq1);
   sdsfree(unq2);
   free(adup);

  return unq_params;
  
 }
  

 static char *get_alpha(const char *txt){

  sds p = sdsempty();

  if(!isoperator(txt) && !isnumeric(txt)){
      
      p = sdscat(p, txt);
      p = sdscat(p, ".");      

     }

      char *chp = 0;

     if(sdslen(p) > 0){

       chp = malloc(sdslen(p)+1);
       ysl_strlcpy(chp, p, sdslen(p)+1);

      }  

  sdsfree(p);
  return chp;    
 }

 static char *inspect(const char *atm){
   
   char *new_atom = 0;

   if(ysl_wc(atm) < 2){
   
   sds n_atom = sdsempty();   
   int optr = oat_hash(atm) % 1000;
   
    switch(optr){ /*special case operators*/
	
 	case cns: n_atom = sdscat(n_atom, ": "); break;
	case at:  /*fallthrough*/
	case lat:
	case fi:  /*n_atom = sdscat(n_atom, " ");*/   break;
	case hed: n_atom = sdscat(n_atom, "Hd "); break;
	case tal: n_atom = sdscat(n_atom, "Tl "); break;
        case lpr:
        case rpr: n_atom = sdscat(n_atom, atm); break;

	default:  n_atom = sdscat(n_atom, atm);  n_atom = sdscat(n_atom, " "); //insert the space neeeded, return a copy of the string
                  
         }         
                  new_atom = malloc(sdslen(n_atom)+1);
	          ysl_strlcpy(new_atom, n_atom, sdslen(n_atom)+1);//the empty space is not always copied without adding another byte
                  sdsfree(n_atom);
       } 
     
     
     return new_atom;
 }

 
 static char *inspect_params(const char *nd){

 char *pargs = 0; 
 
  if(ysl_wc(nd) < 2){

   int optr = oat_hash(nd) % 1000;
   
    switch(optr){ /*special case operators*/
	
 	case cns:  /*fallthrough*/ 
	case def: 
	case at: 
	case lat:
	case fi:  
	case hed: 
	case tal: 
	case nli: 
	case nvl: 
	case flt:
	case tni:
         /*parentheses*/
        case lpr:
        case rpr: break;
	
	default:  /*ps = sdsnew(get_alpha(nd));    //find params here,
	          pargs = malloc(sdslen(ps)+1);
	          ysl_strlcpy(pargs, ps, sdslen(ps)+1); 
                  */
                  pargs = get_alpha(nd);
         }
              
       } 
   
    return pargs;
 }

 static lambda *inspect_sexp(const node *sexp){

 lambda *expr = (lambda*) malloc(sizeof(lambda));
 expr->lm_list = NULL;
 
  if(sexp){

   sds tmp_prms = sdsempty();
        char *d = 0; char *exp = 0;

    while(sexp){ 
      sds dta = sdsnew(sexp->data);
      sds dt1 = sdsnew(sexp->data);

       d = inspect_params(dta);                            //find lambda parameters
      if(d){tmp_prms = sdscat(tmp_prms, d); free(d);}  

       exp = inspect(dt1); 				//insect and perform relevant transformation here	       
      if(exp[0] != '\0') expr->lm_list = prepend(expr->lm_list, list(exp)); 
      
      //printf("dta:%s dt1:%s d:%s exp:%s\n", dta,dt1,d,exp);

      sexp = sexp->next;
      sdsfree(dta); sdsfree(dt1); free(exp); d = 0;
    }     
     
      expr->lm_list = reverse(expr->lm_list);

      sds tmp = sdsnew(get_unique_alpha(tmp_prms));

      expr->args = malloc(sdslen(tmp)+1);
      ysl_strlcpy(expr->args, tmp, sdslen(tmp)+1);

    sdsfree(tmp_prms); sdsfree(tmp);
  }
	
    return expr;
 }


 static node *gen_atoms(const char *yir){
 
 node *tail = NULL; node* head;
 char *yirdup;
 const char* delims = "()";

 if(yir){

    int sz = strlen(yir)+1;
    yirdup = malloc(sz);
    ysl_strlcpy(yirdup, yir, sz);


   sds *tokens;
   int count, j;

   sds line = sdsnew(yirdup);
   tokens = sdssplitlen(line,sdslen(line)," ",1,&count); // split on space
  
   for (j = 0; j < count; j++){

       
      char *par_type = strrchr(tokens[j], '(') ? "(" : ")";  //cond ? true : false //determine the parentheses type
      int ptsz = strlen(par_type)+1;

      const char *s = tokens[j]; 

      do {
        size_t field_len = strcspn(s, delims);

        char *true_atom;

        if(field_len){
          true_atom = malloc(field_len+1);
          sprintf(true_atom, "%.*s", (int)field_len, s);

         }else {
           true_atom = malloc(ptsz);
           ysl_strlcpy(true_atom, par_type, ptsz);  
 
         }        

        //printf("%s ", true_atom);
        tail = prepend(tail, list(true_atom));  
        
        s += field_len;
       } while (*s++);  
   
    }
  sdsfreesplitres(tokens,count);
  free(yirdup);

  head = reverse(tail); 

  }

  return head;
 }

 
 static char *merge(const lambda *expr, const bool isy){ //flatten char list and merge with params to create lambda string

 char *lstr = 0;
 sds chrch = sdsempty();
 chrch = sdscat(chrch, expr->args);
 sds chrch2 = sdsempty();
 node *lst = expr->lm_list;


 while(lst){
     if(lst->next && (strcmp(lst->next->data, ")") == 0 || strcmp(lst->next->data, "(") == 0)){

         sds tchrch = sdsnew(lst->data);
         sdstrim(tchrch, " ");
         chrch2 = sdscat(chrch2, tchrch);
         sdsfree(tchrch);
     }else{

    chrch2 = sdscat(chrch2, lst->data);//flatten

  }
    lst = lst->next;
  }

  //printf("%s\n", chrch2);
  sdsrange(chrch2, 1, -2); 
  
  sds ctmp = sdsdup(chrch2);
  char *lprc = strrchr(ctmp, ')');
  int pso = lprc - ctmp;
  sdsrange(chrch2, 0, pso);

  if(isy){chrch2 = sdscat(chrch2, ")");}

  sdsrange(ctmp, pso, -1);
  chrch2 = sdscat(chrch2, ctmp);
  chrch = sdscat(chrch, chrch2);

  sds chrch3 = sdsempty();

  if(isy){
     chrch3 = sdscat(chrch3, "(Y(");
  }else{
     chrch3 = sdscat(chrch3, "(");
   }

  chrch3 = sdscat(chrch3, chrch);
  
  //printf("%s\n", chrch2);

  lstr = malloc(sdslen(chrch3)+1);
  ysl_strlcpy(lstr, chrch3, sdslen(chrch3)+1);
  
  //printf("%s\n", lstr);
  sdsfree(chrch); sdsfree(chrch2); sdsfree(chrch3); sdsfree(ctmp);
 
  return lstr;
 }


 static node *gen_lambda(node *yir, bool isY){

 node *lmbd = NULL;  char *lmbd_str = 0;

 if(yir){
    do{
      node *atoms = gen_atoms(yir->data);
      lambda *new_exp = inspect_sexp(atoms); 

       lmbd_str = merge(new_exp, isY); 
       //printf("\n%s\n", lmbd_str);
      lmbd = prepend(lmbd, list(lmbd_str));
 
      yir = yir->next;
  }while(yir);
 } 

 //while(lmbd){printf("%s ", lmbd->data); lmbd = lmbd->next;}
 free(lmbd_str);
 return lmbd;

 }

#endif
