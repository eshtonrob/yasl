#define INTEGER_LITERAL 257
#define FLOAT_LITERAL 258
#define ID 259
#define BOOLEAN_LITERAL 260
#define NIL 261
#define IF 262
#define THEN 263
#define ELSE 264
#define DO 265
#define END 266
#define CASE 267
#define LET 268
#define DEF 269
#define HEAD 270
#define TAIL 271
#define PAIR 272
#define NUL 273
#define CIFR 274
#define MATCH 275
#define COLON 276
#define ALL 277
#define INT 278
#define FLOAT 279
#define BOOL 280
#define TOINT 281
#define TOFLOAT 282
#define IN 283
#define INB 284
#define INH 285
#define OUT 286
#define OUTB 287
#define OUTH 288
#define HALT 289
#define LPAR 290
#define RPAR 291
#define LBRAC 292
#define RBRAC 293
#define UMINUS 294
#define INIT 295
#define UPLUS 296
#define EQ 297
#define NOTEQ 298
#define LT 299
#define GT 300
#define AND 301
#define OR 302
#define PLUS 303
#define MINUS 304
#define TIMES 305
#define DIVIDE 306
#define MODULO 307
#define FPLUS 308
#define FMINUS 309
#define FTIMES 310
#define FDIVIDE 311
#ifdef YYSTYPE
#undef  YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
#endif
#ifndef YYSTYPE_IS_DECLARED
#define YYSTYPE_IS_DECLARED 1
typedef union{
	int int_val;
	double real_val;
	bool bool_val;
	char * id;
	tree * a_tree;
 } YYSTYPE;
#endif /* !YYSTYPE_IS_DECLARED */
extern YYSTYPE yylval;
