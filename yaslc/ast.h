#ifndef AST_H
#define AST_H

#include "tree.h"
#include "list.h"
#include "sds/sds.h"
#include "heading.h"


/*
* tree.h builds a parse tree from the input.
* the functions here operate on the resulting tree
* typechecking and transformations happen here
*/

 hashmap **sm; //global symbol table
 
 static void type_check(tree *stmt_s){

  // printtree(stmt_s,1);
  //printf("Types do not match");
 }



 static char *leaf_to_yir(tree *p4){

  sds exp_str = sdsempty(); 
 
  switch(p4->nodetype){
	case int_node:	    exp_str = sdscatsds(exp_str, sdsfromlonglong(p4->body.a_int));
                            break;
	           
	case float_node:    exp_str = sdscat(exp_str, ftos(p4->body.a_real));
                            break;

	case bool_node:     exp_str = sdscat(exp_str, btos(p4->body.a_bool));
                            break;

	case variable_node: exp_str = sdscat(exp_str, p4->body.a_variable);
                            break;
	case operator_node:       break;		   

	} 
   
      char *yir = malloc(sdslen(exp_str)+1);
      ysl_strlcpy(yir,exp_str,sdslen(exp_str)+1);
      sdsfree(exp_str);

    return yir;   
 }



 static char *extract_list(tree *p){
   char *lir = 0;
   sds elst = sdsempty(); 

  if(p){

   if(p->nodetype != operator_node){ return leaf_to_yir(p);}
  
			
   	elst = sdscat(elst, extract_list(p->body.an_operator.left));
   	elst = sdscat(elst, " ");
   	elst = sdscat(elst, extract_list(p->body.an_operator.right));
	  lir = malloc(sdslen(elst)+1);
  	ysl_strlcpy(lir, elst, sdslen(elst)+1);
	sdsfree(elst);
	return lir; 
  }

  return lir; 
 }


 static char *exp_to_yir(tree *p3){
	
 const char *func_call = 0;


 if(p3->nodetype != operator_node){ return leaf_to_yir(p3); }

 sds exp_str  = sdsempty();
 exp_str = sdscat(exp_str,"(");

 int optr = oat_hash(p3->body.an_operator.operator) % 1000;

 switch(optr){ /*match node patterns here*/
     case pls:  /*fallthrough*/
     case mns:
     case tim:
     case mod: 
     case div:  

     case fpls:  /*fallthrough*/
     case fmns:
     case ftim:
     case fdiv: 
     
     /*logical operators*/
     case gtd:   /*fallthrough*/
     case lsd:
     case lor:
     case nte:
     case lan:
     case leq: 
     /*IO Builtins*/
     case ni:
     case nih:
     case nib:
     case tuo:
     case tuoh:
     case tuob:
     case tlah:
     /*Misc ops*/
     case fi:  
     case cns:
     case lat: 
     //case at:
	       exp_str = sdscat(exp_str, p3->body.an_operator.operator);
	       exp_str = sdscat(exp_str, " ");
	       exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.left));
	       exp_str = sdscat(exp_str, " ");
           exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.right));
	       break; 

     case at: exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.right));
              break;

     /*Type cast functions expect only right tree*/
     case flt: exp_str = sdscat(exp_str, "FLT_");
	       exp_str = sdscat(exp_str, " ");
	       exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.right)); 
	       break;

     case tni: exp_str = sdscat(exp_str, "INT_");
	       exp_str = sdscat(exp_str, " ");
	       exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.right)); 
	       break;

     case els: 
     case dat: exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.left));
	           exp_str = sdscat(exp_str, " ");
               exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.right));
	       break; 

     
     case exl: exp_str = sdscat(exp_str, extract_list(p3));
               break; 

     /*list functions expect only right tree*/
     case nli: exp_str = sdscat(exp_str, "NIL"); break;

     case nvl: exp_str = sdscat(exp_str, "NUL");
	       exp_str = sdscat(exp_str, " ");
	       exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.right));  
               break;
    
     case hed: exp_str = sdscat(exp_str, "HEAD");
	       exp_str = sdscat(exp_str, " ");
	       exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.right)); 
               break;

     case tal: exp_str = sdscat(exp_str, "TAIL");
	       exp_str = sdscat(exp_str, " ");
	       exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.right)); 
               break;

             /*not a builtin function or operator, expect function call */
     default :
                 func_call = sym_hashmap_get((*sm), p3->body.an_operator.operator);
                
                 if(func_call){
                    if(strcmp(func_call, "func") != 0){
                         exp_str = sdscat(exp_str, func_call);
			                   exp_str = sdscat(exp_str, " ");
	                       exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.left));
                     }else{
                         exp_str = sdscat(exp_str, p3->body.an_operator.operator);
			                   exp_str = sdscat(exp_str, " ");
	                       exp_str = sdscat(exp_str, exp_to_yir(p3->body.an_operator.left));
			}          
		
                 }else{
                  printf("Undefined reference to symbol:%s\n", p3->body.an_operator.operator);
                  exit(1);
                 }		 
    }

    exp_str = sdscat(exp_str, ")");
    
    char *yir = malloc(sdslen(exp_str)+1);
    ysl_strlcpy(yir,exp_str, sdslen(exp_str)+1);
    
    sdsfree(exp_str); 
                  
    return yir; 
 }
 

 static void sym_update(tree *p){
  
 char *func_str = '\0';  

  if(p){
   if(p->nodetype == operator_node){
           
      int optr = oat_hash(p->body.an_operator.operator) % 1000;

     switch(optr){
             
     case pls:  /*fallthrough*/
     case mns:
     case tim:
     case mod: 
     case div:  
     case fpls:  /*fallthrough*/
     case fmns:
     case ftim:
     case fdiv:  
     case gtd:   /*fallthrough*/
     case lsd:
     case lor:
     case nte:
     case lan:
     case leq:
     case fi:  
     case cns:
     case lat: 
     case at:
     case flt:
     case tni:
     case els: 
     case dat:
     case exl:
     case nvl:
     case nli:
     case hed:
     case tal: break;
     default :  if(p->body.an_operator.right){
                  func_str = exp_to_yir(p->body.an_operator.right);                      //create yir of return expression
                  //printf("%s\n", func_str);
                  sym_hashmap_update((*sm), p->body.an_operator.operator, func_str);     //copy yir func definition to symbol table
              }
        }
    }
  }
 }

 static tree *gen_subtree(tree **ast){ /*Genereate subtrees from the giant parse tree */
 
 tree *result = (tree*) malloc (sizeof(tree));
 
 if(ast){
	//printf("%s\n",ast->body.an_operator.operator);
   switch((*ast)->nodetype){
     
        case int_node:	    /*fallthrough*/
	case float_node:
	case bool_node: 
	case variable_node: result = (*ast); break;

        case operator_node: if(strcmp((*ast)->body.an_operator.operator, ";;") == 0){/*Implies more subtrees will be available*/ 
				result = (*ast)->body.an_operator.right; 
				(*ast) = (*ast)->body.an_operator.left;}

			    else{ result = (*ast); (*ast)= NULL;}/*Smallest possible subtree is current tree*/
			    break;				   /*return and nullify original*/

        }
 
  }
 
 return result;

 }


 static char *callgraph(tree *t, int level){

  sds callstack = sdsempty();
  const int stepp = 4;

  if (t)
     switch (t->nodetype)
     {
       case operator_node:
        callstack = sdscat(callstack,callgraph(t->body.an_operator.right, level+stepp));
         callstack = sdscat(callstack,"#");
        callstack = sdscat(callstack, t->body.an_operator.operator);
         callstack = sdscat(callstack,"#");
        callstack = sdscat(callstack, callgraph(t->body.an_operator.left, level+stepp));
         //callstack = sdscat(callstack,"#");
        break;
       case int_node:
       case float_node:
       case bool_node:
       case variable_node:  break;
     }

    char *yir = malloc(sdslen(callstack)+1);
    ysl_strlcpy(yir,callstack, sdslen(callstack)+1);
    
    sdsfree(callstack); 
    return yir;
 }

 static bool recursive_sym(const char *stack){

  sds cstac = sdsnew(stack); sds *calls;
  int count, j; bool rc = false;

  calls = sdssplitlen(cstac, sdslen(cstac), "#",1,&count);

  for(j = 0; j < count; j++){
    const char *idkey = sym_hashmap_get((*sm), calls[j]); 
     if(idkey){ 
     char *pos = strstr(idkey, calls[j]); //printf("%s - %s -> %s\n", calls[j],pos,idkey);    
     if(pos){rc = true; }
     }
   }
 
  sdsfreesplitres(calls,count);
  return rc;
 }
 
 static node *gen_yir(tree *ast, hashmap *sym, bool *ystatus){
 
 sm = &sym; //assign the global pointer
 tree *sub, *code; 
 node *tail = NULL;
 char *yir; int idx = 1;

  while(ast){/*symbol update*/
   sub = gen_subtree(&ast); //printtree(sub, 1); printf("\n-+-\n");
   sym_update(sub); 

   if(idx){code = sub; idx = 0;} // save pointer to first subtree, this is assumed main
     
    }

   char *g = callgraph(code,1); //build call stack
   *ystatus = recursive_sym(g); //check recursiveness

   yir = exp_to_yir(code);   

   if(strcmp(yir, "()") != 0){
     //printf("%s\n", yir);
     tail = prepend(tail, list(yir)); 
     
   }
 
   /*
   struct hashmap_iter *iter;
   for (iter = hashmap_iter((*sm)); iter; iter = hashmap_iter_next((*sm), iter)) {
    printf("id[%s]: %s \n", sym_hashmap_iter_get_key(iter),  sym_hashmap_iter_get_data(iter));
   }*/
    

   //tail = reverse(tail);

   free(sub);
   free(yir);
   return tail;
 }

#endif

