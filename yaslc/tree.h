#ifndef TREE_H
#define TREE_H 

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "util.h"


enum treetype {operator_node, int_node, float_node, variable_node, bool_node/*, list_node*/};
enum types {t_int, t_float, t_bool, t_none, t_uknwn, t_user}; //t_user is for possible user defined types 

 typedef struct tree {
   enum treetype nodetype;
   enum types s_type;
   union {
     struct {struct tree *left, *right; char *operator;} an_operator;
     int a_int;
     double a_real;
     bool a_bool;
     char *a_variable;
     /*void *lnode;*/
   } body;
 } tree;


static enum types infer(tree *exp);
static enum types expect(tree *exp);


 static enum types expect(tree *exp){//tree walking type inference
   enum types s_t = t_uknwn; enum types tmp;
   int optr = oat_hash(exp->body.an_operator.operator) % 1000;

    switch(optr){ /*match node patterns here*/
     case pls:  /*fallthrough*/
     case mns:
     case tim:
     case mod: /*mod is always a integer operation*/
     case div: s_t = t_int; break;

     case fpls:  /*fallthrough*/
     case fmns:
     case ftim:
     case fdiv: s_t = t_float; break;
     
     /*Type cast functions*/
     case flt: s_t = t_float; break;
     case tni: s_t = t_int; break;

     /*logical operators*/
     case gtd:   /*fallthrough*/
     case lsd:
     case lor:
     case nte:
     case lan:
     case leq: s_t = t_bool; break;

     case at:  /*fallthrough*/
     case dat:
     //case stl:
     case exl: s_t = t_none; break; //type-less node, not a function node, type is t_none

     /*list functions*/
     /*case nli: */
     case nvl: s_t = t_bool; break;
    
     case hed:
     case tal: s_t = infer(exp->body.an_operator.right); break;//infer type from right subtree

     /*For cons */
     case cns: tmp = infer(exp->body.an_operator.right);
	       if (tmp == infer(exp->body.an_operator.left)){s_t = tmp;}
	       else{printf("List items of different types\n");}	
	       break;

     default: s_t = exp->s_type; break;//Accept reported function return type
    }

   return s_t;
 }

 static enum types infer(tree *exp){ //simple type inference 
   enum types s_t = t_uknwn;

    if(exp){

     switch(exp->nodetype){

	case int_node:	    /*fallthrough*/
	case float_node:
	case bool_node: 
	case variable_node : s_t = exp->s_type; break;
	case operator_node : s_t = expect(exp); //walk subtree to infer
     }

  }
  return s_t;
 }


/*
 static tree *make_node(void *node){
    tree *result = (tree*) malloc (sizeof(tree));
    result->nodetype= list_node;
    result->body.lnode=node;
 }
*/
 static tree *make_operator (tree *l, char *o, tree *r) {
   tree *result = (tree*) malloc (sizeof(tree));

   int num_chars = strlen(o)+1;
   result->body.an_operator.operator = malloc(num_chars);
   ysl_strlcpy(result->body.an_operator.operator, o, num_chars);

   result->nodetype= operator_node;
   result->body.an_operator.left= l;
   result->body.an_operator.right= r;
   return result;
 }

 static tree *make_int (int n) {
   tree *result = (tree*) malloc (sizeof(tree));
   result->nodetype= int_node;
   result->s_type= t_int;
   result->body.a_int= n;
   return result;
 }

 static tree *make_real (double n) {
   tree *result = (tree*) malloc (sizeof(tree));
   result->nodetype= float_node;
   result->s_type= t_float;
   result->body.a_real= n;
   return result;
 }

 static tree *make_variable (char *v) {
   tree *result = (tree*) malloc (sizeof(tree));

   int num_chars = strlen(v)+1;
   result->body.a_variable = malloc(num_chars);
   ysl_strlcpy(result->body.a_variable, v, num_chars);

   result->nodetype= variable_node;
   return result;
 }

 static tree *make_bool(bool b){
   tree *result = (tree*) malloc (sizeof(tree));
   result->nodetype= bool_node;
   result->s_type= t_bool;
   result->body.a_bool= b;
   return result;
 }
 
 static tree *make_if(tree *l, tree *r){
   return make_operator(l, "IF", r);
 }

 
 static tree *make_if_else(tree *l, tree *r, tree *alt){
   return make_operator(make_if(l,r), "ELSE", alt);
 }

 static tree *make_cons(tree *l, tree * r){
   return make_operator(l, "CONS", r);
 }

 static tree *make_alias(char *var, tree *exp){
   tree *lid = make_variable(var);
   lid->s_type = infer(exp);
   tree *result = make_operator(lid, "LET", exp);
   result->s_type = lid->s_type;
   return result;
 }


 static void printtree (tree *t, int level) {/*For Debug purposes*/
 #define step 4
   if (t)
     switch (t->nodetype)
     {
       case operator_node:
        printtree (t->body.an_operator.right, level+step);
        printf ("%*c%s\n", level, ' ', t->body.an_operator.operator);
        printtree (t->body.an_operator.left, level+step);
        break;
       case int_node:
        printf ("%*c%d\n", level, ' ', t->body.a_int);
        break;
       case float_node:
        printf ("%*c%g\n", level, ' ', t->body.a_real);
        break;
       case bool_node:
        printf ("%*c%s\n", level, ' ', btos(t->body.a_bool));
        break;
       case variable_node:
        printf ("%*c%s\n", level, ' ', t->body.a_variable);

     }
 }



#endif 
